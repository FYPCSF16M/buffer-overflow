shutdown.c:  This shellcode will shut down the system. The shellcode is shown in following code which will shut down the whole system  

adduser.c: This shellcode will add user in system with open, write and close permissions with the name “pwned” and password “$pass$”

killall.c: This shellcode will kill all the process which are running on system and will return a bash shell without any GUI

myshell.c: This shellcode will get shell on our own terminal

shell_bind_tcp.c and shell_reverse_tcp.c: This shellcode will be used to get shell of victim in two different ways.

Every shellcode is explained in detail in Buffer Overflow.pdf 
