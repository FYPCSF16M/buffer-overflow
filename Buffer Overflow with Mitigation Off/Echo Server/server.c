#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
char res[256];
const char* process_request (int s1)
{
  char result[256];
 // int fd=open("payload5",O_RDONLY);
  read (s1, result, 2048);
  strcpy(res,result);
  return res;
}

int
main (int argc, char *argv[])
{
  struct sockaddr_in   server, client;
  socklen_t len = sizeof (struct sockaddr_in);
  int s,s1, ops = 1;
 
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_family = AF_INET;
  server.sin_port = htons(9000);

  s = socket (PF_INET, SOCK_STREAM, 0);
  if ((setsockopt (s, SOL_SOCKET, SO_REUSEADDR, &ops, sizeof(ops))) < 0)
    perror ("pb_server (reuseaddr):");
  bind (s, (struct sockaddr *) &server, sizeof (server));
  listen (s, 10);
  s1 = accept (s, (struct sockaddr *)&client, &len);
  printf ("Connection from %s\n", inet_ntoa (client.sin_addr));
  const char * str =process_request (s1);  
  write (s1,str  , strlen( str));
  close (s1);
  return 0;
}
