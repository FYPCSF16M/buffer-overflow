Buffer overflow is the most popular vulnerability. Buffer overflow exists, when the application tries to store more data, than the allocated memory space. Broadly speaking, this vulnerability exists when the attacker intentionally inputs more data than the application can handle. The overloaded data first fills the buffer, then it overflows the section of memory, allocated for it. Eventually it overwrites another portion of memory that was meant for another purpose, like instruction pointer. Attacker can take advantage of this flaw to control the flow of application and execute payload or crash the application.
Buffer overflow ranks high in the Common Weakness Enumeration (CWE) /SANS Top 25 most dangerous program errors and is specified as CWE-120 under the Common Weakness Enumeration dictionary of weakness types

Echo server has been exploit using buffer overflow on Windows as well as on Linux Platform. 

The scripts written in python are also available in this respository which are needed to exploit Buffer Overflow


